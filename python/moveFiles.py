import sys, os
import glob
import shutil
import easygui


def moveFiles(src, dest):
    try:
        txt_files = glob.glob(src + 'txt')
        rec_files = glob.glob(src + 'rec')
        existentes =glob.glob(dest + '*.txt')
        # print(existentes)
        # easygui.msgbox('teste')
        # print(txt_files)
        for arquivo in rec_files:
            os.remove(arquivo)

        for arquivo in txt_files:            
            shutil.copy(arquivo, dest)
            os.remove(arquivo)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        str(exc_type, fname, exc_tb.tb_lineno)

# moveFiles('C:\\Users\\matheus.araujo\\Documents\\Arquivos ReceitanetBX\\*.', 'C:\\rpa_data\\teste\\18153454000168\\EFD-C\\')